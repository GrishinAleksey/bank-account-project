package com.grishin.form;

import com.grishin.domain.entity.DebitCard;

public class AddDebitForm {
    private String username;
    private DebitCard debitCard;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public DebitCard getDebitCard() {
        return debitCard;
    }

    public void setDebitCard(DebitCard debitCard) {
        this.debitCard = debitCard;
    }
}
