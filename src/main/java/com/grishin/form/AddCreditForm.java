package com.grishin.form;

import com.grishin.domain.entity.Credit;

public class AddCreditForm {

    private Credit credit;
    private Long creditCardId;

    public Credit getCredit() {
        return credit;
    }

    public void setCredit(Credit credit) {
        this.credit = credit;
    }

    public Long getCreditCardId() {
        return creditCardId;
    }

    public void setCreditCardId(Long creditCardId) {
        this.creditCardId = creditCardId;
    }
}
