package com.grishin.form;

import com.grishin.domain.entity.Credit;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

public class CreditWithSumForm {
    private Long id;
    private BigDecimal depositSum;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getDepositSum() {
        return depositSum;
    }

    public void setDepositSum(BigDecimal depositSum) {
        this.depositSum = depositSum;
    }
}
