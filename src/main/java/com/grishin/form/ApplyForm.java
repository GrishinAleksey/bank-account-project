package com.grishin.form;

import com.grishin.domain.entity.ApplyCreditCard;

public class ApplyForm {
    private ApplyCreditCard apply;
    private String username;

    public ApplyCreditCard getApply() {
        return apply;
    }

    public void setApply(ApplyCreditCard apply) {
        this.apply = apply;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
