package com.grishin.form;

import com.grishin.domain.entity.Account;

public class AddAccountForm {
    private String username;
    private Account account;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
