package com.grishin.form;

import com.grishin.domain.entity.CreditCard;

public class ConfirmCreditCardForm {
    private Long applyId;
    private CreditCard creditCard;

    public Long getApplyId() {
        return applyId;
    }

    public void setApplyId(Long applyId) {
        this.applyId = applyId;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }
}
