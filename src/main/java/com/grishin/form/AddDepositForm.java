package com.grishin.form;

import com.grishin.domain.entity.Deposit;

public class AddDepositForm {
    private Deposit deposit;
    private Long id;

    public Deposit getDeposit() {
        return deposit;
    }

    public void setDeposit(Deposit deposit) {
        this.deposit = deposit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
