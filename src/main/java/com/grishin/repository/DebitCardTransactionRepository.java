package com.grishin.repository;

import com.grishin.domain.entity.DebitCard;
import com.grishin.domain.entity.DebitCardTransaction;
import com.grishin.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DebitCardTransactionRepository extends JpaRepository<DebitCardTransaction, Long> {
    List<DebitCardTransaction> findAllByDebitCard(DebitCard debit);

    @Query(value = "SELECT * FROM debit_transaction WHERE date > (NOW() - INTERVAL '1 HOUR') AND sum >= 100000", nativeQuery=true)
    List<DebitCardTransaction> findByLargeTransaction(User user);

    @Query(value = "SELECT * FROM debit_transaction WHERE status=1", nativeQuery=true)
    List<DebitCardTransaction> findSuspiciousTransaction();
}
