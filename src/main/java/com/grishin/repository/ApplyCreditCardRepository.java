package com.grishin.repository;

import com.grishin.domain.entity.ApplyCreditCard;
import com.grishin.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ApplyCreditCardRepository extends JpaRepository<ApplyCreditCard, Long> {
    List<ApplyCreditCard> findAllByUser(User user);
    ApplyCreditCard findByUser(User user);
}
