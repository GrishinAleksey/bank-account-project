package com.grishin.repository;

import com.grishin.domain.entity.DepositTerm;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepositTermRepository extends JpaRepository<DepositTerm, Long> {
}
