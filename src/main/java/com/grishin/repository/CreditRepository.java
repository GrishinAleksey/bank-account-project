package com.grishin.repository;

import com.grishin.domain.entity.Credit;
import com.grishin.domain.entity.CreditCard;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CreditRepository extends JpaRepository<Credit, Long> {
    List<Credit> findAllByCreditCard(CreditCard card);
}
