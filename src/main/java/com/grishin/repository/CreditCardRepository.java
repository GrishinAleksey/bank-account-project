package com.grishin.repository;

import com.grishin.domain.entity.CreditCard;
import com.grishin.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CreditCardRepository extends JpaRepository<CreditCard, Long> {
    List<CreditCard> findAllByUser(User user);
}
