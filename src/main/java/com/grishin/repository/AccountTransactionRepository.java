package com.grishin.repository;

import com.grishin.domain.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AccountTransactionRepository extends JpaRepository<AccountTransaction, Long> {
    List<AccountTransaction> findAllByAccount(Account account);

    @Query(value = "SELECT * FROM account_transaction WHERE date > (NOW() - INTERVAL '1 HOUR') AND sum >= 100000", nativeQuery=true)
    List<AccountTransaction> findByLargeTransaction(User user);

    @Query(value = "SELECT * FROM account_transaction WHERE status=1", nativeQuery=true)
    List<AccountTransaction> findSuspiciousTransaction();
}
