package com.grishin.repository;

import com.grishin.domain.entity.Deposit;
import com.grishin.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DepositRepository extends JpaRepository<Deposit, Long> {
    List<Deposit> findAllByUser(User user);
}
