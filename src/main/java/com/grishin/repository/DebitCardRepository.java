package com.grishin.repository;

import com.grishin.domain.entity.DebitCard;
import com.grishin.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DebitCardRepository extends JpaRepository<DebitCard, Long> {
    List<DebitCard> findAllByUser(User user);
    DebitCard findByCardNumber(String cardNumber);
}
