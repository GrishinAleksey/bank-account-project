package com.grishin.repository;

import com.grishin.domain.entity.Account;
import com.grishin.domain.entity.DebitCard;
import com.grishin.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Long> {
    List<Account> findAllByUser(User user);
    Account findByName(String name);
}
