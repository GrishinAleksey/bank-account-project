package com.grishin.exception;

public class ExcessDepositBalanceException extends RuntimeException{
    public ExcessDepositBalanceException(String message) {
        super(message);
    }
}
