package com.grishin.exception;

public class DepositNotCloseableException extends RuntimeException{
    public DepositNotCloseableException(String message) {
        super(message);
    }
}
