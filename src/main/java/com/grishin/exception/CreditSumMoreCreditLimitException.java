package com.grishin.exception;

public class CreditSumMoreCreditLimitException extends RuntimeException{
    public CreditSumMoreCreditLimitException(String message) {
        super(message);
    }
}
