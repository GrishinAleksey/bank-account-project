package com.grishin.exception;

public class CreditOverdueException extends RuntimeException{
    public CreditOverdueException(String message) {
        super(message);
    }
}
