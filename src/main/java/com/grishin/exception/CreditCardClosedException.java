package com.grishin.exception;

public class CreditCardClosedException extends RuntimeException{
    public CreditCardClosedException(String message) {
        super(message);
    }
}
