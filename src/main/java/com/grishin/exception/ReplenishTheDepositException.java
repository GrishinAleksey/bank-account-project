package com.grishin.exception;

public class ReplenishTheDepositException extends RuntimeException{
    public ReplenishTheDepositException(String message) {
        super(message);
    }
}
