package com.grishin.controller;

import com.grishin.domain.entity.Credit;
import com.grishin.domain.entity.Deposit;
import com.grishin.exception.DepositNotCloseableException;
import com.grishin.exception.ExcessDepositBalanceException;
import com.grishin.exception.ReplenishTheDepositException;
import com.grishin.form.AddDepositForm;
import com.grishin.form.DepositWithSumForm;
import com.grishin.service.impl.DepositServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
public class DepositController {

    private final DepositServiceImpl depositServiceImpl;

    @Autowired
    public DepositController(DepositServiceImpl depositServiceImpl) {
        this.depositServiceImpl = depositServiceImpl;
    }

    @ModelAttribute("depositForm")
    public Deposit getDeposit() {
        return new Deposit();
    }

    @ModelAttribute("addDepositForm")
    public AddDepositForm addDepositForm() {
        return new AddDepositForm();
    }

    @GetMapping("/add-deposit")
    public String addDeposit(Model model) {
        model.addAttribute("allTerms", depositServiceImpl.getAllTerms());
        model.addAttribute("new-deposit", new Credit());
        return "add-deposit";
    }

    @PostMapping("/add-deposit")
    public String addDeposit(@ModelAttribute("addDepositForm") AddDepositForm addDepositForm, Principal principal, Model model) {
        model.addAttribute("newDeposit", addDepositForm);
        depositServiceImpl.addDeposit(principal.getName(), addDepositForm.getDeposit());
        return "redirect:/deposits";
    }

    @GetMapping("/deposits")
    public String depositList(Principal user, Model model) {
        model.addAttribute("allDeposits", depositServiceImpl.findAllDeposits(user.getName()));
        model.addAttribute("allTerms", depositServiceImpl.getAllTerms());
        return "deposits";
    }

    @GetMapping("/close")
    public String closeDeposit(@RequestParam(name = "depositId") Long depositId) throws DepositNotCloseableException {
        depositServiceImpl.closeDeposit(depositId);
        return "redirect:/deposits";
    }

    @GetMapping("/refill-deposit")
    public String refillDeposit(Model model) {
        model.addAttribute("refillDeposit", new DepositWithSumForm());
        return "refill-deposit";
    }

    @PostMapping("/refill-deposit")
    public String depositCredit(@ModelAttribute("depositWithSum") DepositWithSumForm depositWithSumForm, Model model) throws ReplenishTheDepositException {
        model.addAttribute("depositWithSumForm", depositWithSumForm);
        depositServiceImpl.refillDeposit(depositWithSumForm.getId(), depositWithSumForm.getSum());
        return "redirect:/deposits";
    }

    @GetMapping("/withdraw-deposit")
    public String withdrawCredit(Model model) {
        model.addAttribute("withdrawDeposit", new DepositWithSumForm());
        return "withdraw-deposit";
    }

    @PostMapping("/withdraw-deposit")
    public String withdrawMoney(@ModelAttribute("depositWithSum") DepositWithSumForm depositWithSumForm) throws ExcessDepositBalanceException, ReplenishTheDepositException {
        depositServiceImpl.withdrawMoney(depositWithSumForm.getId(), depositWithSumForm.getSum());
        return "redirect:/deposits";
    }
}
