package com.grishin.controller;

import com.grishin.domain.entity.DebitCard;
import com.grishin.form.AddDebitForm;
import com.grishin.service.impl.DebitCardServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

@Controller
public class DebitCardController {

    private final DebitCardServiceImpl debitCardServiceImpl;

    @Autowired
    public DebitCardController(DebitCardServiceImpl debitCardServiceImpl) {
        this.debitCardServiceImpl = debitCardServiceImpl;
    }

    @ModelAttribute("addDebitForm")
    public AddDebitForm addDebitForm() {
        return new AddDebitForm();
    }

    @GetMapping("/add-debit")
    public String addDebit(Model model) {
        model.addAttribute("new-deposit", new DebitCard());
        return "add-debit";
    }

    @PostMapping("/add-debit")
    public String addDeposit(@ModelAttribute("addDebitForm") AddDebitForm addDebitForm, Principal principal) {
        debitCardServiceImpl.addDebit(principal.getName(), addDebitForm.getDebitCard());
        return "redirect:/debit-cards";
    }
}
