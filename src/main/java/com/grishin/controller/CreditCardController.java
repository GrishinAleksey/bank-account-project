package com.grishin.controller;

import com.grishin.domain.entity.CreditCard;
import com.grishin.form.ConfirmCreditCardForm;
import com.grishin.service.impl.CreditCardServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
public class CreditCardController {

    private final CreditCardServiceImpl creditCardServiceImpl;

    @Autowired
    public CreditCardController(CreditCardServiceImpl creditCardServiceImpl) {
        this.creditCardServiceImpl = creditCardServiceImpl;
    }

    @ModelAttribute("confirmCreditCardForm")
    public ConfirmCreditCardForm getConfirmCreditCard() {
        return new ConfirmCreditCardForm();
    }

    @GetMapping("/credit-card")
    public String creditCard(Model model) {
        model.addAttribute("newCard", new CreditCard());
        return "credit-card";
    }

    @GetMapping("/credit-cards")
    public String creditCardList(Principal user, Model model) {
        model.addAttribute("allCreditCards", creditCardServiceImpl.allCreditCards(user.getName()));
        return "credit-cards";
    }

    @PostMapping("/credit-card")
    public String confirmCreditCard(@ModelAttribute("confirmCreditCardForm") ConfirmCreditCardForm creditCardForm, Model model) {
        model.addAttribute("confirmCreditCardForm", "Кредитная карта");
        creditCardServiceImpl.confirmCreditCard(creditCardForm.getApplyId(), creditCardForm.getCreditCard().getCreditCardCap(),
                creditCardForm.getCreditCard().getCreditCardPercent(), creditCardForm.getCreditCard().getCreditCardDate());
        return "redirect:/admin-applies";
    }

    @GetMapping("/refuse")
    public String refuseCard(@RequestParam(name="cardId") Long cardId) {
        creditCardServiceImpl.refuseCreditCard(cardId);
        return "redirect:/credit-cards";
    }
}
