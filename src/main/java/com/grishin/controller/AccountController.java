package com.grishin.controller;

import com.grishin.domain.entity.Account;
import com.grishin.form.AddAccountForm;
import com.grishin.service.impl.AccountServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

@Controller
public class AccountController {

    private final AccountServiceImpl accountServiceImpl;

    @Autowired
    public AccountController(AccountServiceImpl accountServiceImpl) {
        this.accountServiceImpl = accountServiceImpl;
    }

    @ModelAttribute("addAccountForm")
    public AddAccountForm addAccountForm() {
        return new AddAccountForm();
    }

    @GetMapping("/add-account")
    public String addAccount(Model model) {
        model.addAttribute("new-account", new Account());
        return "add-account";
    }

    @PostMapping("/add-account")
    public String addAccount(@ModelAttribute("addAccountForm") AddAccountForm addAccountForm, Principal principal) {
        accountServiceImpl.addAccount(principal.getName(), addAccountForm.getAccount());
        return "redirect:/accounts";
    }
}
