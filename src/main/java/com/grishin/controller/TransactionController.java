package com.grishin.controller;

import com.grishin.form.TransactionForm;
import com.grishin.service.impl.AccountTransactionServiceImpl;
import com.grishin.service.impl.CardToAccountTransactionServiceImpl;
import com.grishin.service.impl.CardTransactionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.security.Principal;

@Controller
public class TransactionController {

    private final CardTransactionServiceImpl cardTransactionServiceImpl;
    private final AccountTransactionServiceImpl accountTransactionServiceImpl;
    private final CardToAccountTransactionServiceImpl cardToAccountTransactionServiceImpl;

    @Autowired
    public TransactionController(CardTransactionServiceImpl cardTransactionServiceImpl,
                                 AccountTransactionServiceImpl accountTransactionServiceImpl, CardToAccountTransactionServiceImpl cardToAccountTransactionServiceImpl) {
        this.cardTransactionServiceImpl = cardTransactionServiceImpl;
        this.accountTransactionServiceImpl = accountTransactionServiceImpl;
        this.cardToAccountTransactionServiceImpl = cardToAccountTransactionServiceImpl;
    }

    @GetMapping("/debit-cards")
    public String userDebitCards(Principal principal, Model model) {
        model.addAttribute("allDebitCard", cardTransactionServiceImpl.allDebits(principal.getName()));
        return "debit-cards";
    }

    @GetMapping("/debit-transactions")
    public String debitCardTransactionsList(@RequestParam(name = "id") Long cardId, Model model) {
        model.addAttribute("allDebitCardTransactions", cardTransactionServiceImpl.allOperationsOfCard(cardId));
        return "debit-transactions";
    }

    @GetMapping("/card-to-card")
    public String cardToCard(Model model) {
        model.addAttribute("newTransaction", new TransactionForm());
        return "card-to-card";
    }

    @PostMapping("/card-to-card")
    public String cardToCardTransaction(@ModelAttribute("transactionForm") TransactionForm transactionForm, Principal principal, Model model) {
        cardTransactionServiceImpl.fromCardToCard(principal.getName(), transactionForm.getFrom(), transactionForm.getTo(), transactionForm.getSum());
        return "redirect:/debit-cards";
    }

    @GetMapping("/suspicious-transaction")
    public String suspiciousTransaction(Model model) {
//        model.addAttribute("allSuspiciousAccTransactions", accountTransactionService.allSuspiciousTransactions());
        model.addAttribute("allSuspiciousAccTransactions", accountTransactionServiceImpl.allSuspiciousTransactions());
        model.addAttribute("allSuspiciousTransactions", cardTransactionServiceImpl.allSuspiciousTransactions());
        return "suspicious-transaction";
    }

    @GetMapping("/apply-transaction")
    public String applyDebitTransaction(@RequestParam(name = "transactionId") Long transactionId, @RequestParam(name = "cardNumberFrom") String cardNumberFrom,
                               @RequestParam(name = "cardNumberTo") String cardNumberTo, @RequestParam(name = "sum") BigDecimal sum) {
        cardTransactionServiceImpl.applySuspiciousTransaction(transactionId, cardNumberFrom, cardNumberTo, sum);
        return "redirect:/suspicious-transaction";
    }

    @GetMapping("/reject-transaction")
    public String rejectDepositTransaction(@RequestParam(name = "transactionId") Long transactionId) {
        cardTransactionServiceImpl.rejectSuspiciousTransaction(transactionId);
        return "redirect:/suspicious-transaction";
    }

    @GetMapping("/account-transactions")
    public String accountTransactionsList(@RequestParam(name = "id") Long accountId, Model model) {
        model.addAttribute("allAccountTransactions", accountTransactionServiceImpl.allOperationsOfAccount(accountId));
        return "account-transactions";
    }

    @GetMapping("/accounts")
    public String userAccounts(Principal principal, Model model) {
        model.addAttribute("allAccounts", accountTransactionServiceImpl.allAccounts(principal.getName()));
        return "accounts";
    }

    @GetMapping("/account-to-account")
    public String accountToAccount(Model model) {
        model.addAttribute("newTransaction", new TransactionForm());
        return "account-to-account";
    }

    @PostMapping("/account-to-account")
    public String cardToCardTransaction(@ModelAttribute("transactionForm") TransactionForm transactionForm, Principal principal) {
        accountTransactionServiceImpl.fromAccountToAccount(principal.getName(), transactionForm.getFrom(), transactionForm.getTo(), transactionForm.getSum());
        return "redirect:/accounts";
    }

    @GetMapping("/apply-account-transaction")
    public String applyAccTransaction(@RequestParam(name = "transactionId") Long transactionId, @RequestParam(name = "accountNameFrom") String accountNameFrom,
                               @RequestParam(name = "accountNameTo") String accountNameTo, @RequestParam(name = "sum") BigDecimal sum) {
        accountTransactionServiceImpl.applyTransaction(transactionId, accountNameFrom, accountNameTo, sum);
        return "redirect:/suspicious-transaction";
    }

    @GetMapping("/reject-account-transaction")
    public String rejectAccountTransaction(@RequestParam(name = "transactionId") Long transactionId) {
        accountTransactionServiceImpl.rejectSuspiciousTransaction(transactionId);
        return "redirect:/suspicious-transaction";
    }

    @GetMapping("/card-to-account")
    public String cardToAccount(Model model) {
        model.addAttribute("newTransaction", new TransactionForm());
        return "card-to-account";
    }

    @PostMapping("/card-to-account")
    public String cardToAccountTransaction(@ModelAttribute("transactionForm") TransactionForm transactionForm, Principal principal) {
        cardToAccountTransactionServiceImpl.fromCardToAccount(principal.getName(), transactionForm.getFrom(), transactionForm.getTo(), transactionForm.getSum());
        return "redirect:/debit-cards";
    }

    @GetMapping("/apply-card-to-acc-transaction")
    public String applyCardToAccountTransaction(@RequestParam(name = "transactionId") Long transactionId, @RequestParam(name = "cardNumberFrom") String cardNumberFrom,
                                      @RequestParam(name = "accountNameTo") String accountNameTo, @RequestParam(name = "sum") BigDecimal sum) {
        cardToAccountTransactionServiceImpl.applySuspiciousTransaction(transactionId, cardNumberFrom, accountNameTo, sum);
        return "redirect:/suspicious-transaction";
    }

    @GetMapping("/reject-card-to-acc-transaction")
    public String rejectCardToAccountTransaction(@RequestParam(name = "transactionId") Long transactionId) {
        cardToAccountTransactionServiceImpl.rejectSuspiciousTransaction(transactionId);
        return "redirect:/suspicious-transaction";
    }

}
