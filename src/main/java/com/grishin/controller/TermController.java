package com.grishin.controller;

import com.grishin.domain.entity.DepositTerm;
import com.grishin.form.AddTermForm;
import com.grishin.service.impl.DepositServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class TermController {

    private final DepositServiceImpl depositServiceImpl;

    @Autowired
    public TermController(DepositServiceImpl depositServiceImpl) {
        this.depositServiceImpl = depositServiceImpl;
    }

    @GetMapping("/terms")
    public String allTerms(Model model) {
        model.addAttribute("allTerms", depositServiceImpl.getAllTerms());
        return "terms";
    }

    @GetMapping("/delete")
    public String deleteTerm(@RequestParam(name = "termId") Long termId) {
        depositServiceImpl.deleteTerm(termId);
        return "redirect:/terms";
    }

    @GetMapping("/add-term")
    public String addTerm(Model model) {
        model.addAttribute("termForm", new AddTermForm());
        return "add-term";
    }

    @PostMapping("/add-term")
    public String addTerm(@ModelAttribute("addTerm") AddTermForm addTermForm) {
        depositServiceImpl.addTerm(addTermForm.getMinSum(), addTermForm.getMaxSum(), addTermForm.getPercent());
        return "redirect:/terms";
    }

    @GetMapping("/edit-term/{termId}")
    public String getEditTerm(@PathVariable(name = "termId") Long termId, Model model) {
        model.addAttribute("term", depositServiceImpl.findTerm(termId));
        return "edit-term";
    }

    @PostMapping("/edit-term/{termId}")
    public String editTerm(@ModelAttribute DepositTerm term, @PathVariable(name = "termId") Long termId) {
        depositServiceImpl.editTerm(termId, term);
        return "redirect:/terms";
    }
}
