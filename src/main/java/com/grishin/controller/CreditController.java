package com.grishin.controller;

import com.grishin.domain.entity.Credit;
import com.grishin.form.AddCreditForm;
import com.grishin.form.CreditWithSumForm;
import com.grishin.service.impl.CreditServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CreditController {

    private final CreditServiceImpl creditServiceImpl;

    @Autowired
    public CreditController(CreditServiceImpl creditServiceImpl) {
        this.creditServiceImpl = creditServiceImpl;
    }

    @ModelAttribute("creditForm")
    public Credit getCredit() {
        return new Credit();
    }

    @ModelAttribute("addCreditForm")
    public AddCreditForm addCreditForm() {
        return new AddCreditForm();
    }

    @ModelAttribute("creditWithSumForm")
    public CreditWithSumForm CreditWithSumForm() {
        return new CreditWithSumForm();
    }

    @GetMapping("/credits")
    public String creditList(@RequestParam(name = "cardId") Long cardId, Model model) {
        model.addAttribute("allCredits", creditServiceImpl.getCredits(cardId));
        return "credits";
    }

    @GetMapping("/add-credit")
    public String addCredit(Model model) {
        model.addAttribute("new-credit", new Credit());
        return "add-credit";
    }

    @PostMapping("/add-credit")
    public String addCredit(@ModelAttribute("addCreditForm") AddCreditForm addCreditForm, Model model) throws Exception {
        model.addAttribute("newCredit", addCreditForm);
        creditServiceImpl.applyCredit(addCreditForm.getCreditCardId(), addCreditForm.getCredit());
        return "redirect:/credit-cards";
    }

    @GetMapping("/deposit-credit")
    public String depositCredit(Model model) {
        model.addAttribute("newCredit", new CreditWithSumForm());
        return "deposit-credit";
    }

    @PostMapping("/deposit-credit")
    public String depositCredit(@ModelAttribute("creditWithSumForm") CreditWithSumForm creditWithSumForm, Model model) throws Exception {
        model.addAttribute("deposit", creditWithSumForm);
        creditServiceImpl.depositCredit(creditWithSumForm.getId(), creditWithSumForm.getDepositSum());
        return "redirect:/credit-cards";
    }

    @GetMapping("/withdraw-credit")
    public String withdrawCredit(Model model) {
        model.addAttribute("withdraw-credit", new CreditWithSumForm());
        return "withdraw-credit";
    }


    @PostMapping("/withdraw-credit")
    public String withdrawMoney(@ModelAttribute("creditWithSumForm") CreditWithSumForm creditWithSumForm, Model model) {
        model.addAttribute("withdraw", creditWithSumForm);
        creditServiceImpl.withdrawMoney(creditWithSumForm.getId(), creditWithSumForm.getDepositSum());
        return "redirect:/credit-cards";
    }
}
