package com.grishin.controller;

import com.grishin.domain.entity.ApplyCreditCard;
import com.grishin.form.ApplyForm;
import com.grishin.service.impl.CreditCardServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;


@Controller
public class ApplyController {

    private final CreditCardServiceImpl creditCardServiceImpl;

    @Autowired
    public ApplyController(CreditCardServiceImpl creditCardServiceImpl) {
        this.creditCardServiceImpl = creditCardServiceImpl;
    }

    @ModelAttribute("applyForm")
    public ApplyForm getApplyCredit() {
        return new ApplyForm();
    }

    @GetMapping("/apply")
    public String apply(Model model) {
        model.addAttribute("apply", new ApplyCreditCard());
        return "apply";
    }

    @GetMapping("/admin-applies")
    public String adminApplyList(Model model) {
        model.addAttribute("adminApplies", creditCardServiceImpl.allApplies());
        return "admin-applies";
    }

    @GetMapping("/applies")
    public String applyList(Principal user, Model model) {
        model.addAttribute("allApplies", creditCardServiceImpl.allApplies(user.getName()));
        return "applies";
    }

    @PostMapping("/apply")
    public String creditCardApply(@ModelAttribute("applyForm") ApplyForm apply, Principal principal, Model model) {
        apply.setUsername(principal.getName());
        model.addAttribute("applyCredit", "Заявка на кредитную карту принята");
        creditCardServiceImpl.applyCreditCard(apply.getUsername(), apply.getApply());
        return "redirect:/credit-cards";
    }

    @GetMapping("/reject-apply")
    public String rejectApply(@RequestParam(name="applyId") Long applyId) {
        creditCardServiceImpl.rejectCreditCard(applyId);
        return "redirect:/admin-applies";
    }
}
