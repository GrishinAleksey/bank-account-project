package com.grishin.controller;

import com.grishin.service.impl.CreditCardServiceImpl;
import com.grishin.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdminController {

    private final UserServiceImpl userServiceImpl;
    private final CreditCardServiceImpl creditCardServiceImpl;

    @Autowired
    public AdminController(UserServiceImpl userServiceImpl, CreditCardServiceImpl creditCardServiceImpl) {
        this.userServiceImpl = userServiceImpl;
        this.creditCardServiceImpl = creditCardServiceImpl;
    }

    @GetMapping("/admin")
    public String userList(Model model) {
        model.addAttribute("allUsers", userServiceImpl.allUsers());
        return "admin";
    }

    @PostMapping("/admin")
    public String deleteUser(@RequestParam(required = true, defaultValue = "") Long userId,
                             @RequestParam(required = true, defaultValue = "") String action, Model model) {
        if (action.equals("delete")) {
            userServiceImpl.deleteUser(userId);
        }
        return "redirect:/admin";
    }

    @GetMapping("/admin/gt/{userId}")
    public String gtUser(@PathVariable("userId") Long userId, Model model) {
        model.addAttribute("allUsers", userServiceImpl.usergtList(userId));
        return "admin";
    }
}
