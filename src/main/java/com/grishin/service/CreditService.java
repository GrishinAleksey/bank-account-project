package com.grishin.service;

import com.grishin.domain.entity.Credit;

import java.math.BigDecimal;
import java.util.List;

public interface CreditService {

    void applyCredit(Long cardId, Credit credit);

    void depositCredit(Long id, BigDecimal depositSum);

    void withdrawMoney(Long id, BigDecimal withdrawSum);

    void changeFine(Credit credit, Double fine);

    List<Credit> getCredits(Long cardId);
}
