package com.grishin.service;

import java.math.BigDecimal;

public interface CardToAccountTransactionService {

    void fromCardToAccount(String username, String cardNumberFrom, String accountNameTo, BigDecimal sum);

    void applySuspiciousTransaction(Long transactionId, String cardNumberFrom, String accountNameTo, BigDecimal sum);

    void rejectSuspiciousTransaction(Long transactionId);
}
