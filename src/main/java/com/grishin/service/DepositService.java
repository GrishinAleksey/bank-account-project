package com.grishin.service;

import com.grishin.domain.entity.Deposit;
import com.grishin.domain.entity.DepositTerm;

import java.math.BigDecimal;
import java.util.List;

public interface DepositService {

    void addDeposit(String username, Deposit deposit);

    List<Deposit> findAllDeposits(String username);

    void refillDeposit(Long id, BigDecimal amount);

    void withdrawMoney(Long id, BigDecimal amount);

    void closeDeposit(Long depositId);

    void addTerm(BigDecimal minSum, BigDecimal maxSum, Double percent);

    void deleteTerm(Long termId);

    void editTerm(Long termId, DepositTerm updatedTerm);

    DepositTerm findTerm(Long termId);

    List<DepositTerm> getAllTerms();

}
