package com.grishin.service;

import com.grishin.domain.entity.DebitCard;
import com.grishin.domain.entity.DebitCardTransaction;

import java.math.BigDecimal;
import java.util.List;

public interface CardTransactionService {

    void fromCardToCard(String username, String cardNumberFrom, String cardNumberTo, BigDecimal sum);

    void applySuspiciousTransaction(Long transactionId, String cardNumberFrom, String cardNumberTo, BigDecimal sum);

    void rejectSuspiciousTransaction(Long transactionId);

    List<DebitCard> allDebits(String username);

    List<DebitCardTransaction> allOperationsOfCard(Long id);

    List<DebitCardTransaction> allSuspiciousTransactions();
}
