package com.grishin.service;

import com.grishin.domain.entity.ApplyCreditCard;
import com.grishin.domain.entity.CreditCard;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface CreditCardService {

    void applyCreditCard(String username, ApplyCreditCard apply);

    void confirmCreditCard(Long applyId, BigDecimal creditCardCap, Double creditCardPercent, Date creditCardDate);

    void rejectCreditCard(Long applyId);

    void refuseCreditCard(Long cardId);

    List<CreditCard> allCreditCards(String username);

    List<ApplyCreditCard> allApplies(String username);

    List<ApplyCreditCard> allApplies();
}
