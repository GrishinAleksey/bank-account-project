package com.grishin.service;

import com.grishin.domain.entity.Account;

public interface AccountService {
    void addAccount(String username, Account account);
}
