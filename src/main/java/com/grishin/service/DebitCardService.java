package com.grishin.service;

import com.grishin.domain.entity.DebitCard;

public interface DebitCardService {

    void addDebit(String username, DebitCard debitCard);
}
