package com.grishin.service;

import com.grishin.domain.entity.Account;
import com.grishin.domain.entity.AccountTransaction;

import java.math.BigDecimal;
import java.util.List;

public interface AccountTransactionService {

    void fromAccountToAccount(String username, String accountNameFrom, String accountNameTo, BigDecimal sum);

    void applyTransaction(Long transactionId, String accountNameFrom, String accountNameTo, BigDecimal sum);

    void rejectSuspiciousTransaction(Long transactionId);

    List<AccountTransaction> allOperationsOfAccount(Long id);

    List<Account> allAccounts(String username);

    List<AccountTransaction> allSuspiciousTransactions();
}
