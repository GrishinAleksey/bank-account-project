package com.grishin.service.impl;

import com.grishin.domain.entity.Deposit;
import com.grishin.domain.entity.DepositTerm;
import com.grishin.domain.entity.User;
import com.grishin.exception.DepositNotCloseableException;
import com.grishin.exception.ExcessDepositBalanceException;
import com.grishin.exception.ReplenishTheDepositException;
import com.grishin.repository.DepositRepository;
import com.grishin.repository.DepositTermRepository;
import com.grishin.repository.UserRepository;
import com.grishin.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class DepositServiceImpl implements DepositService {

    private final DepositRepository depositRepository;
    private final UserRepository userRepository;
    private final DepositTermRepository termRepository;

    @Autowired
    public DepositServiceImpl(DepositRepository depositRepository, UserRepository userRepository, DepositTermRepository termRepository) {
        this.depositRepository = depositRepository;
        this.userRepository = userRepository;
        this.termRepository = termRepository;
    }

    @Override
    public void addDeposit(String username, Deposit deposit) {
        User user = userRepository.findByUsername(username);
        deposit.setUser(user);
        depositRepository.save(deposit);
    }

    @Override
    public List<Deposit> findAllDeposits(String username) {
        User user = userRepository.findByUsername(username);
        return depositRepository.findAllByUser(user);
    }

    @Override
    public void refillDeposit(Long id, BigDecimal amount) throws ReplenishTheDepositException {
        Deposit deposit = depositRepository.findById(id).get();
        if (!deposit.getRefillable()) {
            throw new ReplenishTheDepositException("Вклад не пополняем");
        }
        deposit.setCash(deposit.getCash().add(amount));
        depositRepository.save(deposit);
    }

    @Override
    public void withdrawMoney(Long id, BigDecimal amount) throws ExcessDepositBalanceException, ReplenishTheDepositException {
        Deposit deposit = depositRepository.findById(id).get();
        if (!deposit.getWithdrawal()) {
            throw new ReplenishTheDepositException("Вклад не доступен для снятия средств");
        }
        if (deposit.getCash().compareTo(amount) < 0) {
            throw new ExcessDepositBalanceException("Вы ввели сумму превышающую баланс вклада");
        }
        deposit.setCash(deposit.getCash().subtract(amount));
        depositRepository.save(deposit);
    }

    @Override
    public void closeDeposit(Long depositId) throws DepositNotCloseableException {
        Deposit deposit = depositRepository.findById(depositId).get();
        if (!deposit.getClosable()) {
            throw new DepositNotCloseableException("Условия вклада не позволяют закрывать его досрочно");
        }
        depositRepository.delete(deposit);
    }

    @Override
    public void addTerm(BigDecimal minSum, BigDecimal maxSum, Double percent) {
        DepositTerm term = new DepositTerm();
        term.setMinSum(minSum);
        term.setMaxSum(maxSum);
        term.setDepositPercent(percent);
        termRepository.save(term);
    }

    @Override
    public void deleteTerm(Long termId) {
        DepositTerm term = termRepository.findById(termId).get();
        termRepository.delete(term);
    }

    @Override
    public void editTerm(Long termId, DepositTerm updatedTerm) {
        DepositTerm term = termRepository.findById(termId).get();
        term.setMinSum(updatedTerm.getMinSum());
        term.setMaxSum(updatedTerm.getMaxSum());
        term.setDepositPercent(updatedTerm.getDepositPercent());
        termRepository.save(term);
    }

    @Override
    public DepositTerm findTerm(Long termId) {
        return termRepository.findById(termId).get();
    }

    @Override
    public List<DepositTerm> getAllTerms() {
        return termRepository.findAll();
    }
}
