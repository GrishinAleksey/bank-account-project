package com.grishin.service.impl;

import com.grishin.domain.entity.DebitCard;
import com.grishin.domain.entity.User;
import com.grishin.repository.DebitCardRepository;
import com.grishin.repository.UserRepository;
import com.grishin.service.DebitCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DebitCardServiceImpl implements DebitCardService {

    private final DebitCardRepository debitCardRepository;
    private final UserRepository userRepository;

    @Autowired
    public DebitCardServiceImpl(DebitCardRepository debitCardRepository, UserRepository userRepository) {
        this.debitCardRepository = debitCardRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void addDebit(String username, DebitCard debitCard) {
        User user = userRepository.findByUsername(username);
        debitCard.setUser(user);
        debitCardRepository.save(debitCard);
    }

}
