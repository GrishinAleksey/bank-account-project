package com.grishin.service.impl;

import com.grishin.domain.entity.*;
import com.grishin.domain.enums.TransactionStatus;
import com.grishin.repository.*;
import com.grishin.service.CardTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class CardTransactionServiceImpl implements CardTransactionService {

    private final DebitCardTransactionRepository debitCardTransactionRepository;
    private final DebitCardRepository debitCardRepository;
    private final UserRepository userRepository;

    @Autowired
    public CardTransactionServiceImpl(DebitCardTransactionRepository debitCardTransactionRepository, DebitCardRepository debitCardRepository, UserRepository userRepository) {
        this.debitCardTransactionRepository = debitCardTransactionRepository;
        this.debitCardRepository = debitCardRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    @Override
    public void fromCardToCard(String username, String cardNumberFrom, String cardNumberTo, BigDecimal sum) {

        DebitCard cardFrom = debitCardRepository.findByCardNumber(cardNumberFrom);
        DebitCard cardTo = debitCardRepository.findByCardNumber(cardNumberTo);

        DebitCardTransaction debitCardTransaction = new DebitCardTransaction(cardFrom, cardNumberTo, sum, new Date());

        cardFrom.setCash(cardFrom.getCash().subtract(sum));
        cardTo.setCash(cardTo.getCash().add(sum));
        //todo Добавить фрод-мониторинг

        if (sum.compareTo(new BigDecimal(50000)) > 0 &&
                debitCardTransactionRepository.findByLargeTransaction(userRepository.findByUsername(username)).size() > 2) {
            debitCardTransaction.setStatus(TransactionStatus.SUSPICIOUS);
        } else {
            debitCardRepository.save(cardFrom);
            debitCardRepository.save(cardTo);
            debitCardTransaction.setStatus(TransactionStatus.SUCCESS);
        }
        debitCardTransactionRepository.save(debitCardTransaction);
    }

    @Transactional
    @Override
    public void applySuspiciousTransaction(Long transactionId, String cardNumberFrom, String cardNumberTo, BigDecimal sum) {
        DebitCardTransaction transaction = debitCardTransactionRepository.findById(transactionId).get();
        transaction.setStatus(TransactionStatus.SUCCESS);

        DebitCard cardFrom = debitCardRepository.findByCardNumber(cardNumberFrom);
        DebitCard cardTo = debitCardRepository.findByCardNumber(cardNumberTo);

        cardFrom.setCash(cardFrom.getCash().subtract(sum));
        cardTo.setCash(cardTo.getCash().add(sum));

        debitCardTransactionRepository.save(transaction);
        debitCardRepository.save(cardFrom);
        debitCardRepository.save(cardTo);
    }

    @Transactional
    @Override
    public void rejectSuspiciousTransaction(Long transactionId) {
        DebitCardTransaction transaction = debitCardTransactionRepository.findById(transactionId).get();
        transaction.setStatus(TransactionStatus.LOCKED);
        debitCardTransactionRepository.save(transaction);
    }

    @Override
    public List<DebitCard> allDebits(String username) {
        User user = userRepository.findByUsername(username);
        return debitCardRepository.findAllByUser(user);
    }

    @Override
    public List<DebitCardTransaction> allOperationsOfCard(Long id) {
        DebitCard debitCard = debitCardRepository.findById(id).get();
        return debitCardTransactionRepository.findAllByDebitCard(debitCard);
    }

    @Override
    public List<DebitCardTransaction> allSuspiciousTransactions () {
        return debitCardTransactionRepository.findSuspiciousTransaction();
    }
}
