package com.grishin.service.impl;

import com.grishin.domain.entity.Account;
import com.grishin.domain.entity.User;
import com.grishin.repository.AccountRepository;
import com.grishin.repository.UserRepository;
import com.grishin.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final UserRepository userRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, UserRepository userRepository) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void addAccount(String username, Account account) {
        User user = userRepository.findByUsername(username);
        account.setUser(user);
        accountRepository.save(account);
    }

}
