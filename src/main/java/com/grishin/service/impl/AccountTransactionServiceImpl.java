package com.grishin.service.impl;

import com.grishin.domain.entity.*;
import com.grishin.domain.enums.TransactionStatus;
import com.grishin.repository.*;
import com.grishin.service.AccountTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class AccountTransactionServiceImpl implements AccountTransactionService {

    private final AccountTransactionRepository accountTransactionRepository;
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;

    @Autowired
    public AccountTransactionServiceImpl(AccountTransactionRepository accountTransactionRepository, AccountRepository accountRepository, UserRepository userRepository) {
        this.accountTransactionRepository = accountTransactionRepository;
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    @Override
    public void fromAccountToAccount(String username, String accountNameFrom, String accountNameTo, BigDecimal sum) {

        Account accountFrom = accountRepository.findByName(accountNameFrom);
        Account accountTo = accountRepository.findByName(accountNameTo);

        AccountTransaction accountTransaction = new AccountTransaction(accountFrom, accountNameTo, sum, new Date());

        accountFrom.setCash(accountFrom.getCash().subtract(sum));
        accountTo.setCash(accountTo.getCash().add(sum));

        if (sum.compareTo(new BigDecimal(50000)) > 0 &&
                accountTransactionRepository.findByLargeTransaction(userRepository.findByUsername(username)).size() > 2) {
            accountTransaction.setStatus(TransactionStatus.SUSPICIOUS);
        } else {
            accountRepository.save(accountFrom);
            accountRepository.save(accountTo);
            accountTransaction.setStatus(TransactionStatus.SUCCESS);
        }
        accountTransactionRepository.save(accountTransaction);
    }

    @Transactional
    @Override
    public void applyTransaction(Long transactionId, String accountNameFrom, String accountNameTo, BigDecimal sum) {
        AccountTransaction transaction = accountTransactionRepository.findById(transactionId).get();
        transaction.setStatus(TransactionStatus.SUCCESS);

        Account accountFrom = accountRepository.findByName(accountNameFrom);
        Account accountTo = accountRepository.findByName(accountNameTo);

        accountFrom.setCash(accountFrom.getCash().subtract(sum));
        accountTo.setCash(accountTo.getCash().add(sum));

        accountTransactionRepository.save(transaction);
        accountRepository.save(accountFrom);
        accountRepository.save(accountTo);
    }

    @Transactional
    @Override
    public void rejectSuspiciousTransaction(Long transactionId) {
        AccountTransaction transaction = accountTransactionRepository.findById(transactionId).get();
        transaction.setStatus(TransactionStatus.LOCKED);
        accountTransactionRepository.save(transaction);
    }

    @Override
    public List<AccountTransaction> allOperationsOfAccount(Long id) {
        Account account = accountRepository.findById(id).get();
        return accountTransactionRepository.findAllByAccount(account);
    }

    @Override
    public List<Account> allAccounts(String username) {
        User user = userRepository.findByUsername(username);
        return accountRepository.findAllByUser(user);
    }

    @Override
    public List<AccountTransaction> allSuspiciousTransactions () {
        return accountTransactionRepository.findSuspiciousTransaction();
    }
}
