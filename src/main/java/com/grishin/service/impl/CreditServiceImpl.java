package com.grishin.service.impl;

import com.grishin.domain.entity.Credit;
import com.grishin.domain.entity.CreditCard;
import com.grishin.domain.enums.CardStatus;
import com.grishin.domain.enums.CreditStatus;
import com.grishin.domain.enums.Currency;
import com.grishin.exception.CreditCardClosedException;
import com.grishin.exception.CreditOverdueException;
import com.grishin.exception.CreditSumMoreCreditLimitException;
import com.grishin.exception.InsufficientFundsException;
import com.grishin.repository.CreditCardRepository;
import com.grishin.repository.CreditRepository;
import com.grishin.service.CreditService;
import com.grishin.utils.DateConverting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class CreditServiceImpl implements CreditService {

    private final CreditRepository creditRepository;
    private final CreditCardRepository cardRepository;

    @Autowired
    public CreditServiceImpl(CreditRepository creditRepository, CreditCardRepository cardRepository) {
        this.creditRepository = creditRepository;
        this.cardRepository = cardRepository;
    }

    @Override
    public void applyCredit(Long cardId, Credit credit) {

        CreditCard creditCard = cardRepository.findById(cardId).get();
        List<Credit> credits = creditRepository.findAllByCreditCard(creditCard);
        BigDecimal sumOfAllCredits = new BigDecimal(0).add(credit.getSumOfCredit());

        if (creditCard.getCardStatus().equals(CardStatus.CLOSED)) {
            throw new CreditCardClosedException("Кредитная карта закрыта");
        }

        for (Credit creditIter : credits) {
            if (creditIter.getCreditDate().before(new Date())) {
                throw new CreditOverdueException("На кредитной карте имеются просроченные кредиты");
            }
            sumOfAllCredits = sumOfAllCredits.add(creditIter.getSumOfCredit());
            if (sumOfAllCredits.compareTo(creditCard.getCreditCardCap()) > 0) {
                throw new CreditSumMoreCreditLimitException("Сумма всех кредитов превышает лимит карты");
            }
        }

        Date creditDate = DateConverting.convert(creditCard.getCreditCardDate());
        credit.setCreditDate(creditDate);
        credit.setDepositedSumOfCredit(new BigDecimal(0));
        credit.setCreditCard(creditCard);
        credit.setCreditStatus(CreditStatus.OPEN);
        credit.setFine(0D);
        creditRepository.saveAndFlush(credit);

    }

    @Override
    public void depositCredit(Long id, BigDecimal depositSum) {
        Credit credit = creditRepository.findById(id).get();

        if (depositSum.compareTo(credit.getSumOfCredit().subtract(credit.getDepositedSumOfCredit())) > 0) {
            throw new InsufficientFundsException("Введенная сумма больше оставшейся суммы кредита");
        }
        if (credit.getSumOfCredit().equals(credit.getDepositedSumOfCredit().add(depositSum))) {
            credit.setCreditStatus(CreditStatus.CLOSED);
        }
        credit.setDepositedSumOfCredit(credit.getDepositedSumOfCredit().add(depositSum));
        creditRepository.save(credit);
    }

    @Override
    public void withdrawMoney(Long id, BigDecimal withdrawSum) {
        Credit credit = creditRepository.findById(id).get();
        if (credit.getDepositedSumOfCredit().compareTo(withdrawSum) < 0) {
            throw new InsufficientFundsException("Недостаточно средств");
        }
        if (credit.getCurrency().equals(Currency.DOLLAR)) {
            withdrawSum = withdrawSum.divideToIntegralValue(new BigDecimal(75));
        }
        if (credit.getCurrency().equals(Currency.EURO)) {
            withdrawSum = withdrawSum.divideToIntegralValue(new BigDecimal(85));
        }
        credit.setDepositedSumOfCredit(credit.getDepositedSumOfCredit().subtract(withdrawSum));
        creditRepository.save(credit);
    }

    @Override
    public void changeFine(Credit credit, Double fine) {
        credit.setFine(fine);
        creditRepository.saveAndFlush(credit);
    }

    @Override
    public List<Credit> getCredits(Long cardId) {
        CreditCard creditCard = cardRepository.findById(cardId).get();
        return creditRepository.findAllByCreditCard(creditCard);
    }
}
