package com.grishin.service.impl;

import com.grishin.domain.entity.ApplyCreditCard;
import com.grishin.domain.entity.CreditCard;
import com.grishin.domain.entity.User;
import com.grishin.domain.enums.CardStatus;
import com.grishin.repository.ApplyCreditCardRepository;
import com.grishin.repository.CreditCardRepository;
import com.grishin.repository.UserRepository;
import com.grishin.service.CreditCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class CreditCardServiceImpl implements CreditCardService {

    private final ApplyCreditCardRepository applyCreditCardRepository;
    private final CreditCardRepository cardRepository;
    private final UserRepository userRepository;

    @Autowired
    public CreditCardServiceImpl(ApplyCreditCardRepository applyCreditCardRepository, CreditCardRepository cardRepository, UserRepository userRepository) {
        this.applyCreditCardRepository = applyCreditCardRepository;
        this.cardRepository = cardRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void applyCreditCard(String username, ApplyCreditCard apply) {
        User user = userRepository.findByUsername(username);
        apply.setStatus("Заявка");
        apply.setUser(user);
        applyCreditCardRepository.save(apply);
    }

    @Override
    public void confirmCreditCard(Long applyId, BigDecimal creditCardCap, Double creditCardPercent, Date creditCardDate) {
        ApplyCreditCard apply = applyCreditCardRepository.findById(applyId).get();
        apply.setStatus("Одобрено");
        CreditCard creditCard = new CreditCard();
        creditCard.setCreditCardCap(creditCardCap);
        creditCard.setCreditCardPercent(creditCardPercent);
        creditCard.setCreditCardDate(creditCardDate);
        creditCard.setUser(apply.getUser());
        creditCard.setCardStatus(CardStatus.OPEN);
        applyCreditCardRepository.save(apply);
        cardRepository.save(creditCard);
    }

    @Override
    public void rejectCreditCard(Long applyId) {
        ApplyCreditCard apply = applyCreditCardRepository.findById(applyId).get();
        apply.setStatus("Отклонено");
        applyCreditCardRepository.save(apply);
    }

    @Override
    public void refuseCreditCard(Long cardId) {
        CreditCard creditCard = cardRepository.findById(cardId).get();
        creditCard.setCardStatus(CardStatus.CLOSED);
        cardRepository.save(creditCard);
    }

    @Override
    public List<CreditCard> allCreditCards(String username) {
        User user = userRepository.findByUsername(username);
        return cardRepository.findAllByUser(user);
    }

    @Override
    public List<ApplyCreditCard> allApplies(String username) {
        User user = userRepository.findByUsername(username);
        return applyCreditCardRepository.findAllByUser(user);
    }

    @Override
    public List<ApplyCreditCard> allApplies(){
        return applyCreditCardRepository.findAll();
    }
}