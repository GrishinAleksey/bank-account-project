package com.grishin.service.impl;

import com.grishin.domain.entity.Account;
import com.grishin.domain.entity.DebitCard;
import com.grishin.domain.entity.DebitCardTransaction;
import com.grishin.domain.enums.TransactionStatus;
import com.grishin.repository.AccountRepository;
import com.grishin.repository.DebitCardRepository;
import com.grishin.repository.DebitCardTransactionRepository;
import com.grishin.repository.UserRepository;
import com.grishin.service.CardToAccountTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class CardToAccountTransactionServiceImpl implements CardToAccountTransactionService {

    private final DebitCardTransactionRepository debitCardTransactionRepository;
    private final DebitCardRepository debitCardRepository;
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;

    @Autowired
    public CardToAccountTransactionServiceImpl(DebitCardTransactionRepository debitCardTransactionRepository,
                                               DebitCardRepository debitCardRepository, AccountRepository accountRepository, UserRepository userRepository) {
        this.debitCardTransactionRepository = debitCardTransactionRepository;
        this.debitCardRepository = debitCardRepository;
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    @Override
    public void fromCardToAccount(String username, String cardNumberFrom, String accountNameTo, BigDecimal sum) {

        DebitCard cardFrom = debitCardRepository.findByCardNumber(cardNumberFrom);
        Account accountTo = accountRepository.findByName(accountNameTo);

        DebitCardTransaction debitCardTransaction = new DebitCardTransaction(cardFrom, accountNameTo, sum, new Date());

        cardFrom.setCash(cardFrom.getCash().subtract(sum));
        accountTo.setCash(accountTo.getCash().add(sum));

        if (sum.compareTo(new BigDecimal(50000)) > 0 &&
                debitCardTransactionRepository.findByLargeTransaction(userRepository.findByUsername(username)).size() > 2) {
            debitCardTransaction.setStatus(TransactionStatus.SUSPICIOUS);
        } else {
            debitCardRepository.save(cardFrom);
            accountRepository.save(accountTo);
            debitCardTransaction.setStatus(TransactionStatus.SUCCESS);
        }
        debitCardTransactionRepository.save(debitCardTransaction);
    }

    @Transactional
    @Override
    public void applySuspiciousTransaction(Long transactionId, String cardNumberFrom, String accountNameTo, BigDecimal sum) {
        DebitCardTransaction transaction = debitCardTransactionRepository.findById(transactionId).get();
        transaction.setStatus(TransactionStatus.SUCCESS);

        DebitCard cardFrom = debitCardRepository.findByCardNumber(cardNumberFrom);
        Account accountTo = accountRepository.findByName(accountNameTo);

        cardFrom.setCash(cardFrom.getCash().subtract(sum));
        accountTo.setCash(accountTo.getCash().add(sum));

        debitCardTransactionRepository.save(transaction);
        debitCardRepository.save(cardFrom);
        accountRepository.save(accountTo);
    }

    @Transactional
    @Override
    public void rejectSuspiciousTransaction(Long transactionId) {
        DebitCardTransaction transaction = debitCardTransactionRepository.findById(transactionId).get();
        transaction.setStatus(TransactionStatus.LOCKED);
        debitCardTransactionRepository.save(transaction);
    }
}
