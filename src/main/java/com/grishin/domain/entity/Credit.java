package com.grishin.domain.entity;

import com.grishin.domain.enums.CardStatus;
import com.grishin.domain.enums.CreditStatus;
import com.grishin.domain.enums.Currency;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "credits")
public class Credit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "sum", nullable = false)
    private BigDecimal sumOfCredit;
    @Column(name = "deposited", columnDefinition = "numeric default 0")
    private BigDecimal depositedSumOfCredit;
    @Column(name = "date")
    private Date creditDate;
    @Enumerated(EnumType.STRING)
    @Column(name = "currency", nullable = false)
    private Currency currency;
    @Column(name = "fine")
    private Double fine;
    @Column(name = "status")
    private CreditStatus creditStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "card_id", nullable = false)
    private CreditCard creditCard;

    public Credit() {
    }

    public Credit(BigDecimal sumOfCredit, Date creditDate, Currency currency) {
        this.sumOfCredit = sumOfCredit;
        this.creditDate = creditDate;
        this.currency = currency;
    }

    public CreditStatus getCreditStatus() {
        return creditStatus;
    }

    public void setCreditStatus(CreditStatus creditStatus) {
        this.creditStatus = creditStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSumOfCredit() {
        return sumOfCredit;
    }

    public void setSumOfCredit(BigDecimal sumOfCredit) {
        this.sumOfCredit = sumOfCredit;
    }

    public Date getCreditDate() {
        return creditDate;
    }

    public void setCreditDate(Date creditDate) {
        this.creditDate = creditDate;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Double getFine() {
        return fine;
    }

    public void setFine(Double fine) {
        this.fine = fine;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public BigDecimal getDepositedSumOfCredit() {
        return depositedSumOfCredit;
    }

    public void setDepositedSumOfCredit(BigDecimal depositedSumOfCredit) {
        this.depositedSumOfCredit = depositedSumOfCredit;
    }
}
