package com.grishin.domain.entity;

import com.grishin.domain.enums.TransactionStatus;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "debit_transaction")
public class DebitCardTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "debit_id", nullable = false)
    private DebitCard debitCard;
    @Column(name = "debit_to", nullable = false)
    private String debitCardTo;
    @Column(name = "sum", nullable = false)
    private BigDecimal sum;
    @Column(name = "status")
    private TransactionStatus status;
    @Column(name = "date")
    private Date date;

    public DebitCardTransaction() {
    }

    public DebitCardTransaction(DebitCard debitCard, String debitCardTo, BigDecimal sum, Date date) {
        this.debitCard = debitCard;
        this.debitCardTo = debitCardTo;
        this.sum = sum;
        this.date = date;
    }

    public String getDebitCardTo() {
        return debitCardTo;
    }

    public void setDebitCardTo(String debitCardTo) {
        this.debitCardTo = debitCardTo;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DebitCard getDebitCard() {
        return debitCard;
    }

    public void setDebitCard(DebitCard debitCard) {
        this.debitCard = debitCard;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

}
