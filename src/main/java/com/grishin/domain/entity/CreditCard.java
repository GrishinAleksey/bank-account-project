package com.grishin.domain.entity;

import com.grishin.domain.enums.CardStatus;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "cards")
public class CreditCard {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "cap")
    private BigDecimal creditCardCap;
    @Column(name = "percent")
    private Double creditCardPercent;
    @Column(name = "date")
    private Date creditCardDate;
    @Column(name = "status")
    private CardStatus cardStatus;

    @OneToMany(mappedBy = "creditCard", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Credit> credits;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "apply_id", referencedColumnName = "id")
    private ApplyCreditCard apply;

    public CreditCard() {
    }


    public CreditCard(BigDecimal creditCardCap, Double creditCardPercent, Date creditCardDate) {
        this.creditCardCap = creditCardCap;
        this.creditCardPercent = creditCardPercent;
        this.creditCardDate = creditCardDate;
    }

    public CardStatus getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(CardStatus cardStatus) {
        this.cardStatus = cardStatus;
    }

    public ApplyCreditCard getApply() {
        return apply;
    }

    public void setApply(ApplyCreditCard apply) {
        this.apply = apply;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCreditCardCap() {
        return creditCardCap;
    }

    public void setCreditCardCap(BigDecimal creditCardCap) {
        this.creditCardCap = creditCardCap;
    }

    public Double getCreditCardPercent() {
        return creditCardPercent;
    }

    public void setCreditCardPercent(Double creditCardPercent) {
        this.creditCardPercent = creditCardPercent;
    }

    public Date getCreditCardDate() {
        return creditCardDate;
    }

    public void setCreditCardDate(Date creditCardDate) {
        this.creditCardDate = creditCardDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Credit> getCredits() {
        return credits;
    }

    public void setCredits(Set<Credit> credits) {
        this.credits = credits;
    }
}
