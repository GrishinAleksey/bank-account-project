package com.grishin.domain.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "deposits")
public class Deposit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private BigDecimal cash;
    @Column(name = "date")
    private Date depositDate;

    private Boolean refillable;
    private Boolean closable;
    private Boolean capitalized;
    private Boolean withdrawal;

    @Column(name = "duration")
    private Date depositDuration;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "term_id", nullable = false)
    private DepositTerm term;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public Deposit() {
    }

    public Deposit(String name, Date depositDate, Boolean refillable, Boolean closable, Boolean capitalized, Boolean withdrawal, DepositTerm term) {
        this.name = name;
        this.depositDate = depositDate;
        this.refillable = refillable;
        this.closable = closable;
        this.capitalized = capitalized;
        this.withdrawal = withdrawal;
        this.term = term;
    }

    public Date getDepositDuration() {
        return depositDuration;
    }

    public void setDepositDuration(Date depositDuration) {
        this.depositDuration = depositDuration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public Date getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(Date depositDate) {
        this.depositDate = depositDate;
    }

    public Boolean getRefillable() {
        return refillable;
    }

    public void setRefillable(Boolean refillable) {
        this.refillable = refillable;
    }

    public Boolean getClosable() {
        return closable;
    }

    public void setClosable(Boolean closable) {
        this.closable = closable;
    }

    public Boolean getCapitalized() {
        return capitalized;
    }

    public void setCapitalized(Boolean capitalized) {
        this.capitalized = capitalized;
    }

    public Boolean getWithdrawal() {
        return withdrawal;
    }

    public void setWithdrawal(Boolean withdrawal) {
        this.withdrawal = withdrawal;
    }

    public DepositTerm getTerm() {
        return term;
    }

    public void setTerm(DepositTerm term) {
        this.term = term;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
