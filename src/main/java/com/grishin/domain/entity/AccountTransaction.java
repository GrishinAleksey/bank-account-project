package com.grishin.domain.entity;

import com.grishin.domain.enums.TransactionStatus;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "account_transaction")
public class AccountTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;
    @Column(name = "account_to", nullable = false)
    private String accountTo;
    @Column(name = "sum", nullable = false)
    private BigDecimal sum;
    @Column(name = "status", nullable = false)
    private TransactionStatus status;
    @Column(name = "date", nullable = false)
    private Date date;

    public AccountTransaction() {
    }

    public AccountTransaction(Account account, String accountTo, BigDecimal sum, Date date) {
        this.account = account;
        this.accountTo = accountTo;
        this.sum = sum;
        this.date = date;
    }

    public String getAccountTo() {
        return accountTo;
    }

    public void setAccountTo(String accountTo) {
        this.accountTo = accountTo;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

}
