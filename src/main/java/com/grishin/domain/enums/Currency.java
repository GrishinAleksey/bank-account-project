package com.grishin.domain.enums;

public enum Currency {
    DOLLAR, EURO, RUBBLE
}