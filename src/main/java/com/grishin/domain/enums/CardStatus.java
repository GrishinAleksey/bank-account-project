package com.grishin.domain.enums;

public enum CardStatus {
    OPEN, EXPECTS, CLOSED
}
