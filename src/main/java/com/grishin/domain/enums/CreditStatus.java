package com.grishin.domain.enums;

public enum  CreditStatus {
    OPEN, CLOSED
}
