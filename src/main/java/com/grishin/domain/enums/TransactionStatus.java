package com.grishin.domain.enums;

public enum  TransactionStatus {
    SUCCESS, SUSPICIOUS, LOCKED
}
