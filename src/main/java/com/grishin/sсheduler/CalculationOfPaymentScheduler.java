package com.grishin.sсheduler;

import com.grishin.domain.entity.Deposit;
import com.grishin.domain.entity.DepositTerm;
import com.grishin.repository.DepositRepository;
import com.grishin.repository.DepositTermRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class CalculationOfPaymentScheduler {

    private static final String CRON = "0 0 10 1-7 * MON";/// Каждый первый понедельник месяца
    private static final String CRON1 = "0 * * * * ?";
    private static final Logger LOG = LoggerFactory.getLogger(CalculationOfPaymentScheduler.class);
    private final DepositRepository depositRepository;
    private final DepositTermRepository depositTermRepository;

    public CalculationOfPaymentScheduler(DepositRepository depositRepository, DepositTermRepository depositTermRepository) {
        this.depositRepository = depositRepository;
        this.depositTermRepository = depositTermRepository;
    }

    @Scheduled(cron = CRON)
    public void calculatePayment() {
        List<Deposit> deposits = depositRepository.findAll();
        for (Deposit deposit : deposits) {
            Long id = deposit.getTerm().getId();
            DepositTerm term = depositTermRepository.findById(id).get();
            BigDecimal minSum = term.getMinSum();
            BigDecimal multiplyPercent = BigDecimal.valueOf(term.getDepositPercent());
            deposit.setCash(deposit.getCash().add(minSum.multiply(multiplyPercent.multiply(new BigDecimal("0.001")))));
            depositRepository.save(deposit);
            LOG.debug("Выпала по ставкам прошла");
        }
    }

}
