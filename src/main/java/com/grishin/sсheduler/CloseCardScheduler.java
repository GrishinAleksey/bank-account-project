package com.grishin.sсheduler;

import com.grishin.domain.entity.Credit;
import com.grishin.domain.entity.CreditCard;
import com.grishin.domain.enums.CardStatus;
import com.grishin.domain.enums.CreditStatus;
import com.grishin.repository.CreditCardRepository;
import com.grishin.repository.CreditRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class CloseCardScheduler {

    private final CreditCardRepository cardRepository;
    private final CreditRepository creditRepository;
    private static final String CRON = "0 * * * * ?";
    private static final Logger LOG = LoggerFactory.getLogger(CloseCardScheduler.class);

    public CloseCardScheduler(CreditCardRepository cardRepository, CreditRepository creditRepository) {
        this.cardRepository = cardRepository;
        this.creditRepository = creditRepository;
    }

    @Scheduled(cron = CRON)
    public void closeCard() {
        List<CreditCard> creditCards = cardRepository.findAll();
        for (CreditCard creditCard : creditCards) {
            if (creditCard.getCreditCardDate().before(new Date())) {
                List<Credit> creditsByCard = creditRepository.findAllByCreditCard(creditCard);
                for (Credit credit : creditsByCard) {
                    if (credit.getCreditStatus().equals(CreditStatus.OPEN)) {
                        LOG.debug("На карте " + creditCard.getId() + " имеются открытые кредитные линии");
                        break;
                    }
                    creditCard.setCardStatus(CardStatus.CLOSED);
                    cardRepository.save(creditCard);
                    LOG.debug("Кредитная карта " + creditCard.getId() + " закрыта");
                }
            }
            LOG.debug("Кредитная карта " + creditCard.getId() + " не закончила свое действие");
        }
    }
}
