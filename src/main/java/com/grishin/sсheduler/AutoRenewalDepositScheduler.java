package com.grishin.sсheduler;

import com.grishin.domain.entity.Deposit;
import com.grishin.repository.DepositRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class AutoRenewalDepositScheduler {

    private static final String CRON = "0 * * * * ?";
    private static final Logger LOG = LoggerFactory.getLogger(AutoRenewalDepositScheduler.class);
    private final DepositRepository depositRepository;

    public AutoRenewalDepositScheduler(DepositRepository depositRepository) {
        this.depositRepository = depositRepository;
    }

    @Scheduled(cron = CRON)
    public void renewalDeposit() {

        List<Deposit> deposits = depositRepository.findAll();

        for (Deposit deposit : deposits) {
            Date depositDuration = deposit.getDepositDuration();
            if (depositDuration.before(new Date())) {

                Calendar instance = Calendar.getInstance();
                instance.setTime(depositDuration);
                instance.add(Calendar.DAY_OF_MONTH, 30);
                Date newDurationDate = instance.getTime();
                deposit.setDepositDuration(newDurationDate);

                depositRepository.save(deposit);
                LOG.debug("Длительность вклада продлена");

            }
        }
    }
}
