package com.grishin.sсheduler;

import com.grishin.domain.entity.Credit;
import com.grishin.domain.enums.CreditStatus;
import com.grishin.repository.CreditRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class FineScheduler {

    private static final String CRON = "0 * * * * ?";
    private static final String CRON1 = "* * 8 * * *";//Каждый день в 8
    private final CreditRepository creditRepository;
    private static final Logger LOG = LoggerFactory.getLogger(FineScheduler.class);

    public FineScheduler(CreditRepository creditRepository) {
        this.creditRepository = creditRepository;
    }

    @Scheduled(cron = CRON)
    public void accrualFine() {
        List<Credit> credits = creditRepository.findAll();
        for (Credit credit : credits) {

            long millisecondsBetween = new Date().getTime() - credit.getCreditDate().getTime();
            int daysBetween = (int) (millisecondsBetween / (24 * 60 * 60 * 1000));

            if (credit.getCreditDate().before(new Date()) && credit.getFine() <= daysBetween && credit.getCreditStatus().equals(CreditStatus.OPEN)) {
                if (credit.getFine() <= 40D) {
                    credit.setFine(credit.getFine() + 1D);
                    LOG.debug("Пени по кредиту " + credit.getId() + " было увеличино в связи с истечением срока кредита");
                    creditRepository.save(credit);
                }
                LOG.debug("Пени по кредиту " + credit.getId() + " достигли максимального значения");
            }
        }
    }
}
