package com.grishin.utils;

import com.grishin.domain.entity.Credit;

import java.math.BigDecimal;
import java.util.Date;


public class CreditSumCalculation {
    public static void calculate(Credit credit, BigDecimal depositSum) {

        long milliseconds = new Date().getTime() - credit.getCreditDate().getTime();
        int days = (int) (milliseconds / (24 * 60 * 60 * 1000));//todo вывести в константу

        if (days > 40) {
            credit.setFine(40D);
        }
        credit.setFine((double) days);
        credit.setSumOfCredit(credit.getSumOfCredit().add(credit.getSumOfCredit().multiply(BigDecimal.valueOf(credit.getFine() * 0.01))));
        credit.setDepositedSumOfCredit(depositSum);
    }
}
