package com.grishin.utils;

import com.grishin.exception.CreditCardExpiredException;

import java.time.LocalDate;
import java.util.Date;

public class DateConverting {

    public static Date convert(Date creditCardDate) throws CreditCardExpiredException {
        LocalDate localDate = LocalDate.now().plusDays(30);
        Date date = java.sql.Date.valueOf(localDate);
        if (creditCardDate.before(date)) {
            long milliseconds = creditCardDate.getTime() - new Date().getTime();
            int days = (int) (milliseconds / (24 * 60 * 60 * 1000));
            if (days < 1) {
                throw new CreditCardExpiredException("До окончания кредитной карты меньше 1 дня");
            }
            return creditCardDate;
        }
        date = java.sql.Date.valueOf(localDate);
        return date;
    }

}
