package com.grishin.utils;

import com.grishin.domain.enums.Currency;

import static com.grishin.domain.enums.Currency.*;

public class CurrencyUtils {

    public static Currency convertStringToCurrency (String currency) {

        switch (currency.toLowerCase()) {
            case "dollar":
                return DOLLAR;
            case "euro":
                return EURO;
            default:
                return RUBBLE;
        }

    }
}